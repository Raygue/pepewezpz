Anggota Kelompok: <br>
Diva Amelia 1806146915 <br>
Harnindyto Wicaksana 1806146991 <br>
Kamila Kaffah 1806191225 <br>
Muhammad Azis Husein 1806191622 <hr>

Link herokuapp = pewlicious.herokuapp.com

Aplikasi yang kita buat adalah aplikasi pencarian restoran. <br>
Aplikasi tersebut menyediakan informasi tentang berbagai macam restoran yang tersedia di beberapa kota di Indonesia. <br>
Manfaat dari aplikasi ini adalah untuk mempermudah dan mempercepat akses seseorang apabila ingin mencari restoran yang sesuai dengan kebutuhannya. <br>
Hubungan dengan industri 4.0 adalah, semua orang mengandalkan internet untuk mendapatkan yang mereka inginkan dengan mudah, cepat, dan otomatis. <br>
Aplikasi pencarian restoran kita ini adalah pilihan yang tepat untuk mencari restoran sesuai dengan kebutuhan di era industri 4.0 ini.

Fitur yang akan diimplementasikan: <br>
-Search Tempat <br>
Di Fitur ini kita dapat memilih kota yang tersedia di drowndown menu. <br>
Setelah memilih kota, akan keluar list restoran yang ada di kota tersebut.<br>
<br>
-Review Tempat <br>
Di Fitur ini kita dapat melihat dan mereview restoran pilihan kita. <br>
Review tersebut dilakukan di form comment. <br>
(Opsional) di fitur ini juga bisa melihat dan mereview dengan bintang. <br>
<br>
-Fitur Like Tempat <br>
Di Fitur ini kita dapat like restoran pilihan kita. <br>
Setelah kita melakukan like, angka like tersebut akan bertambah satu. <br>
<br>
-Book Tempat <br>
Di fitur ini kita dapat book restoran pilihan kita. <br>
Kita dapat book restoran pilihan kita di form yang sudah disediakan. <br>
